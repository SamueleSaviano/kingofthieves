﻿using System.IO;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

public class LevelCreator : EditorWindow
{
    public class Cell
    {
        public int value;
        public int topCell, bottomCell, leftCell, rightCell;
        public bool hasTop, hasBottom, hasLeft, hasRight;
    }

    private List<GameObject> _blocks = new List<GameObject>();

    private PrefabsData _prefabsData;
    private Sprite _dirtSprite;
    private Sprite _terrainSprite;
    private Transform _grid;

    private Sprite _dirtBackgroundSprite;
    private Sprite _backgroundSprite;
    private LevelBackground _levelBackground;

    private GameObject _selectedPrefab;
    private GameObject _clone;
    private Cell _selectedCell;
    private TerrainBlock _terrainBlock;

    private Vector2 _clonePosition;

    private string _csvPath;
    private string _csvLocalPath;

    private string _csvDataDivider = ",";

    private string _levelPreview;

    private string[] _csvLines;
    private string[] _csvLineData;

    private float _blocksSize;

    private int _rows;
    private int _columns;

    private int[,] _matrix;

    private bool _hasTop => this._selectedCell.hasTop;
    private bool _hasBottom => this._selectedCell.hasBottom;
    private bool _hasLeft => this._selectedCell.hasLeft;
    private bool _hasRight => this._selectedCell.hasRight;

    [MenuItem("ZeptoLab/Level Creator")]
    public static void OpenWindow()
    {
        EditorWindow.GetWindow(typeof(LevelCreator), false, "Level Creator");
    }

    private void OnGUI()
    {
        this._prefabsData = (PrefabsData)EditorGUILayout.ObjectField("Prefabs Data", this._prefabsData, typeof(PrefabsData), true);

        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical();
        this._dirtSprite = (Sprite)EditorGUILayout.ObjectField("Dirt Sprite", this._dirtSprite, typeof(Sprite), true);
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        this._terrainSprite = (Sprite)EditorGUILayout.ObjectField("Terrain Sprite", this._terrainSprite, typeof(Sprite), true);
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

        GUILayout.Space(5f);

        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical();
        this._dirtBackgroundSprite = (Sprite)EditorGUILayout.ObjectField("Dirt BKG Sprite", this._dirtBackgroundSprite, typeof(Sprite), true);
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        this._backgroundSprite = (Sprite)EditorGUILayout.ObjectField("BKG Sprite", this._backgroundSprite, typeof(Sprite), true);
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

        this._levelBackground = (LevelBackground)EditorGUILayout.ObjectField("Level Background", this._levelBackground, typeof(LevelBackground), true);

        GUILayout.Space(5f);

        if (GUILayout.Button("Select CSV File"))
        {
            this._csvPath = EditorUtility.OpenFilePanel("Select CSV", "Assets/00KingOfThieves/Files", "csv");
        }

        if (!string.IsNullOrEmpty(this._csvPath))
        {
            this._csvLocalPath = this._csvPath.Replace(Application.dataPath, "Assets");
            GUILayout.Label($"CSV Path: {this._csvLocalPath}");

            GUILayout.Space(5f);

            GUILayout.Label($"File Content:\n\n{File.ReadAllText(this._csvPath)}");

            GUILayout.Space(20f);

            this._csvDataDivider = EditorGUILayout.TextField("CSV Data Divider", this._csvDataDivider);

            GUILayout.Space(5f);

            if(GUILayout.Button("Get Data"))
            {
                this.GetData();
            }
        }

        GUILayout.Space(5f);

        if (!string.IsNullOrEmpty(this._levelPreview))
        {
            GUILayout.Label($"Level Preview:\n\n{this._levelPreview}");

            GUILayout.Space(20f);

            this._grid = (Transform)EditorGUILayout.ObjectField("Grid Parent", this._grid, typeof(Transform), true);

            if(this._grid != null && 
               this._prefabsData != null && 
               this._dirtSprite != null && 
               this._terrainSprite != null)
            {

                GUILayout.Space(5f);

                if(GUILayout.Button("Generate Level"))
                {
                    this.GenerateLevel();
                }
            }
        }

        if (this._grid != null && this._grid.childCount > 0)
        {
            if(this._levelBackground != null &&
               this._dirtBackgroundSprite != null &&
               this._backgroundSprite != null)
            {
                if(GUILayout.Button("Set Background"))
                {
                    this._levelBackground.SetBackground(this._rows * this._blocksSize, this._dirtBackgroundSprite, this._backgroundSprite);
                }
            }

            if(GUILayout.Button("Clear Level"))
            {
                this.ClearLevel();
            }
        }
    }

    private void GetData()
    {
        this._csvLines = File.ReadAllText(this._csvPath).Split("\n"[0]);

        this._rows = this._csvLines.Length;
        this._columns = this._csvLines[0].Split(this._csvDataDivider.ToCharArray()).Length;

        this._matrix = new int[this._rows, this._columns];
        this._levelPreview = string.Empty;

        for (int i = 0; i < this._rows; i++)
        {
            this._csvLineData = this._csvLines[i].Split(this._csvDataDivider.ToCharArray());

            for (int j = 0; j < this._columns; j++)
            {
                if(int.TryParse(this._csvLineData[j], out int o_data))
                {
                    this._matrix[i, j] = o_data;

                    this._levelPreview += (o_data == 1) ? "[1]" : "[_]";
                }
                else
                {
                    Debug.LogError($"An error occurred getting {this._csvLineData[j]} data");
                }

                if(j == this._columns - 1)
                {
                    this._levelPreview += "\n";
                }
            }
        }
    }

    private void GenerateLevel()
    {
        this.ClearLevel();

        if(this._matrix == null)
        {
            this._matrix = new int[this._rows, this._columns];
        }

        this._selectedCell = new Cell();
        this._grid.position = Vector3.zero;

        this._blocksSize = CameraManager.Width / this._columns;

        for (int i = 0; i < this._rows; i++)
        {
            for (int j = 0; j < this._columns; j++)
            {
                this.CreateBlock(i, j);
            }
        }

        this._grid.position = new Vector3(0, -((CameraManager.Height % this._blocksSize) * 0.5f), 0);
        this._grid.GetComponent<LevelGrid>().SetValues(this._columns, this._rows, this._blocksSize);
    }

    private void ClearLevel()
    {
        if (this._grid.childCount > 0)
        {
            for (int i = 0; i < this._grid.childCount; i++)
            {
                this._blocks.Add(this._grid.GetChild(i).gameObject);
            }

            for (int i = 0; i < this._blocks.Count; i++)
            {
                GameObject.DestroyImmediate(this._blocks[i]);
            }

            this._blocks.Clear();
        }
    }

    private string _blockName;

    private void CreateBlock(int p_row, int p_column)
    {
        this._selectedCell.value = this._matrix[p_row, p_column];

        if (this._selectedCell.value == 1)
        {
            this._selectedPrefab = this._prefabsData.terrainBlock;
            this._blockName = $"Terrain ({p_column}_{p_row})";

            this.Instantiate(p_row, p_column);

            this._terrainBlock = this._clone.GetComponent<TerrainBlock>();
            this.SetTerrainBlock(this._terrainBlock, p_row, p_column);
        }
        else
        {
            this._selectedPrefab = this._prefabsData.collectableBlock;
            this._blockName = $"Collectable ({p_column}_{p_row})";

            this.Instantiate(p_row, p_column);
        }
    }

    private void Instantiate(int p_row, int p_column)
    {
        this._clone = (GameObject)PrefabUtility.InstantiatePrefab(this._selectedPrefab);
        this._clone.transform.SetParent(this._grid);
        this._clone.name = this._blockName;

        this._clone.transform.localScale = Vector3.one * this._blocksSize;

        this._clonePosition.x = (CameraManager.HorizontalMin - this._blocksSize * 0.5f) + (this._blocksSize * (p_column + 1));
        this._clonePosition.y = (CameraManager.VerticalMax + this._blocksSize * 0.5f) - (this._blocksSize * (p_row + 1));
        this._clone.transform.position = this._clonePosition;
    }

    private void SetTerrainBlock(TerrainBlock p_block, int p_row, int p_column)
    {
        this._selectedCell.hasTop = p_row - 1 > 0 && this._matrix[p_row - 1, p_column] == 0;
        this._selectedCell.hasBottom = p_row + 1 < this._rows && this._matrix[p_row + 1, p_column] == 0;

        this._selectedCell.hasLeft = p_column - 1 > 0 && this._matrix[p_row, p_column - 1] == 0;
        this._selectedCell.hasRight = p_column + 1 < this._columns && this._matrix[p_row, p_column + 1] == 0;

        p_block.SetColliders(this._hasTop, this._hasBottom, this._hasLeft, this._hasRight);
        p_block.SetSprite(this._hasTop ? this._terrainSprite : this._dirtSprite);
    }
}

#endif