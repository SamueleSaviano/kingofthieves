﻿using UnityEngine;

namespace ZeptoLab.UI
{
    public class GameScreen : MonoBehaviour
    {
        public ScreenType screenType;
        [SerializeField]
        private bool _playAudio = true;

        protected virtual void Awake()
        {
            UIManager.AddScreen(this);
            this.gameObject.SetActive(false);
        }

        public virtual void EnterScreen()
        {
            this.gameObject.SetActive(true);

            if (this._playAudio)
            {
                GameSingleton.AudioManager.PlaySound(AudioType.EnterScreen);
            }
        }

        public virtual void ExitScreen()
        {
            this.gameObject.SetActive(false);
        }
    }
}
