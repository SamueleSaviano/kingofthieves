﻿using UnityEngine;
using UnityEngine.UI;
using ZeptoLab.UI;

public class MainMenu : GameScreen
{
    [SerializeField]
    private Text _bestScoreText;

    public override void EnterScreen()
    {
        base.EnterScreen();
        this._bestScoreText.text = GameManager.BestScore.ToString();
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void LevelsButton()
    {
        UIManager.ChangeScreen(ScreenType.LevelSelection);
    }

    public void PlayButton()
    {
        GameManager.SetUpLevel();
        UIManager.ChangeScreen(ScreenType.HUD);
    }
}
