﻿using UnityEngine;
using UnityEngine.UI;
using ZeptoLab.UI;

public class HUD : GameScreen
{
    [SerializeField]
    private GameObject _waitingPanel;
    [SerializeField]
    private GameObject _playingPanel;

    [Space]

    [SerializeField]
    private RectTransform _scoreIcon;
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private Text _timeText;

    public Vector2 scorePosition => Camera.main.ScreenToWorldPoint(this._scoreIcon.position);

    public override void EnterScreen()
    {
        base.EnterScreen();

        this.UpdateScore();
        this.SwitchPanel(GameState.Waiting);
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    private void SwitchPanel(GameState p_state)
    {
        switch (p_state)
        {
            case GameState.Menu:
            case GameState.Waiting:

                this._waitingPanel.SetActive(true);
                this._playingPanel.SetActive(false);

                break;

            case GameState.Playing:
            case GameState.GameOver:

                this._waitingPanel.SetActive(false);
                this._playingPanel.SetActive(true);

                break;
        }
    }

    public void WaitingPanel()
    {
        GameManager.StartGame();
        this.SwitchPanel(GameState.Playing);
    }

    public void UpdateScore()
    {
        this._scoreText.text = GameManager.Score.ToString();
    }

    public void UpdateTime()
    {
        this._timeText.text = GameManager.Timer.TimeString();
    }
}
