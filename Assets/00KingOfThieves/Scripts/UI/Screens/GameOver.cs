﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using ZeptoLab.UI;

public class GameOver : GameScreen
{
    [SerializeField]
    private Text _scoreText;
    [SerializeField]
    private GameObject _newBestLabel;

    private bool _newBest;

    public override void EnterScreen()
    {
        base.EnterScreen();

        this._scoreText.text = GameManager.Score.ToString();

        this.CheckBestScore();

        if (this._newBest)
        {
            StartCoroutine(this.ShowNewBest());
        }
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    private void CheckBestScore()
    {
        this._newBest = false;
        this._newBestLabel.SetActive(false);

        if (GameManager.Score > GameManager.BestScore)
        {
            GameManager.BestScore = GameManager.Score;
            this._newBest = true;
        }
    }

    private IEnumerator ShowNewBest()
    {
        yield return new WaitForSeconds(0.5f);

        this._newBestLabel.SetActive(true);
        GameSingleton.AudioManager.PlaySound(AudioType.Achievement);
    }

    public void HomeButton()
    {
        GameManager.ResetGame();
        GameManager.SetUpLevel();

        UIManager.ChangeScreen(ScreenType.MainMenu);
    }

    public void ReplayButton()
    {
        GameManager.ResetGame();
        GameManager.SetUpLevel();

        UIManager.ChangeScreen(ScreenType.HUD);
    }
}
