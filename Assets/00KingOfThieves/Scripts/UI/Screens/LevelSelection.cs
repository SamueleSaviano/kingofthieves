﻿using System.Collections.Generic;
using UnityEngine;
using ZeptoLab.UI;

public class LevelSelection : GameScreen
{
    private LevelsData _data;

    [SerializeField]
    private List<LevelSelectionButton> _buttons = new List<LevelSelectionButton>();
    public static LevelSelectionButton SelectedButton;

    private LevelSelectionButton GetButton(int p_level)
    {
        return this._buttons.Find(_x => _x.index == p_level);
    }

    protected override void Awake()
    {
        base.Awake();

        this._data = GameData.GetData<LevelsData>();
    }

    public override void EnterScreen()
    {
        base.EnterScreen();

        for (int i = 0; i < this._buttons.Count; i++)
        {
            this._buttons[i].SetGUI(this._data.GetData(i + 1));
        }

        this.ChangeSelection(this.GetButton(GameManager.CurrentLevel));
    }

    public override void ExitScreen()
    {
        base.ExitScreen();
    }

    public void ChangeSelection(LevelSelectionButton p_button)
    {
        if(LevelSelection.SelectedButton != null)
        {
            LevelSelection.SelectedButton.SetSelected(false);
        }

        LevelSelection.SelectedButton = p_button;
        LevelSelection.SelectedButton.SetSelected(true);
    }

    public void BackButton()
    {
        if (GameManager.CurrentLevel != LevelSelection.SelectedButton.index)
        {
            GameManager.LoadLevel(LevelSelection.SelectedButton.index);
        }

        UIManager.ChangeScreen(ScreenType.MainMenu);
    }
}
