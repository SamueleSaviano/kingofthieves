﻿using UnityEngine;

namespace ZeptoLab.UI
{
    public class Button : MonoBehaviour
    {
        [SerializeField]
        private AudioType _audioType;

        public virtual void OnTap()
        {
            GameSingleton.AudioManager.PlaySound(this._audioType);
        }
    }
}
