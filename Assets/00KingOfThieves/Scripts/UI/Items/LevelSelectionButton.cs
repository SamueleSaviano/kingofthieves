﻿using UnityEngine;
using UnityEngine.UI;
using static GameSingleton;

public class LevelSelectionButton : ZeptoLab.UI.Button
{
    [Space]

    [SerializeField]
    private Color _selectedColor;
    private Color _textStandardColor;

    [Space]

    [SerializeField]
    private Image _back;
    [SerializeField]
    private Image _image;
    [SerializeField]
    private Text _name;

    //[HideInInspector]
    public int index;

    private void Awake()
    {
        this._textStandardColor = this._name.color;
    }

    public void SetGUI(LevelData p_data)
    {
        this._back.color = Color.white;

        this._image.sprite = p_data.image;
        this._name.text = p_data.name;

        this.index = p_data.index;
    }

    public void SetSelected(bool p_selected)
    {
        this._back.color = p_selected ? this._selectedColor : Color.white;
        this._name.color = p_selected ? this._selectedColor : this._textStandardColor;
    }

    public override void OnTap()
    {
        base.OnTap();

        UIManager.GetScreenScript<LevelSelection>().ChangeSelection(this);
    }
}
