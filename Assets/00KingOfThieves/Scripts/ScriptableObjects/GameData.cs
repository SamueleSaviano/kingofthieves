﻿using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Data/Game Data")]
public class GameData : ScriptableObject
{
    public float playDuration;

    [Space]

    [SerializeField]
    private ScriptableObject[] _data;

    public static T GetData<T>() where T : ScriptableObject
    {
        return (T)GameSingleton.GameData._data.ToList().Find(_x => _x.GetType() == typeof(T));
    }
}
