﻿using UnityEngine;

[CreateAssetMenu(fileName = "Layers Data", menuName = "Data/Layers Data")]
public class LayersData : ScriptableObject
{
    public LayerMask terrainLayer;
    public LayerMask wallsLayer;
    public LayerMask coinLayer;
    public LayerMask chestLayer;

    public static int GetMask(LayerMask p_layer)
    {
        return LayerMask.GetMask(LayerMask.LayerToName(p_layer));
    }
}
