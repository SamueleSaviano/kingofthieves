﻿using UnityEngine;

[CreateAssetMenu(fileName = "PrefabsData", menuName = "Data/Prefabs Data")]
public class PrefabsData : ScriptableObject
{
    public GameObject terrainBlock;
    public GameObject collectableBlock;
    public GameObject coin;
    public GameObject chest;
}
