﻿using System.Collections.Generic;
using UnityEngine;
using static GameSingleton;

[CreateAssetMenu(fileName = "CollectablesData", menuName = "Data/Collectables Data")]
public class CollectablesData : ScriptableObject
{
    public float minSpawnTime;
    public float maxSpawnTime;

    [Space]

    public int maxChestsInGame;
    public float minScoreForChestSpawn;

    [Space]

    [SerializeField]
    private List<CollectableItemData> collectables = new List<CollectableItemData>();

    public CollectableItemData GetData(CollectableType p_type)
    {
        return this.collectables.Find(_x => _x.type == p_type);
    }
}
