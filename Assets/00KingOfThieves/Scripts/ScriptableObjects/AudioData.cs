﻿using System.Collections.Generic;
using UnityEngine;
using static GameSingleton;

[CreateAssetMenu(fileName = "AudioData", menuName = "Data/Audio Data")]
public class AudioData : ScriptableObject
{
    [SerializeField]
    private List<GameSound> sounds = new List<GameSound>();

    public GameSound GetSound(AudioType p_type)
    {
        return this.sounds.Find(_x => _x.type == p_type);
    }
}
