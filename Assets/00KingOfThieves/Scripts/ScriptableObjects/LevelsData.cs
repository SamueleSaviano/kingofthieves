﻿using System.Collections.Generic;
using UnityEngine;
using static GameSingleton;

[CreateAssetMenu(fileName = "LevelsData", menuName = "Data/Levels Data")]
public class LevelsData : ScriptableObject
{
    [SerializeField]
    private List<LevelData> _levels = new List<LevelData>();

    public LevelData GetData(int p_level)
    {
        return this._levels.Find(_x => _x.index == p_level);
    }
}
