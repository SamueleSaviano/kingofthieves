﻿using UnityEngine;

public class LevelGrid : MonoBehaviour
{
    public int columns;
    public int rows;
    public float blocksSize;

    private float _desiredScreenWidth => this.blocksSize * this.columns * 0.5f;

    private void Start()
    {
        GameSingleton.CameraManager.ResizeCamera(this._desiredScreenWidth);
    }

    public void SetValues(int p_columns, int p_rows, float p_blockSize)
    {
        this.columns = p_columns;
        this.rows = p_rows;
        this.blocksSize = p_blockSize;
    }
}
