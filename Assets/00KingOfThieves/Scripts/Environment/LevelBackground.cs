﻿using UnityEngine;

public class LevelBackground : MonoBehaviour
{
    [SerializeField]
    private Transform _mask;
    [SerializeField]
    private SpriteRenderer _dirtBkg;
    [SerializeField]
    private SpriteRenderer _background;

    public void SetBackground(float p_maskHeight, Sprite p_dirtBackground, Sprite p_background)
    {
        this._mask.transform.localScale = new Vector2(CameraManager.Width, p_maskHeight);
        this._dirtBkg.sprite = p_dirtBackground;
        this._background.sprite = p_background;
    }
}
