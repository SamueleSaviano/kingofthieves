﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState
{
    Menu,
    Waiting,
    Playing,
    GameOver
}

public class GameManager : MonoBehaviour
{
    private static GameState GameState;

    public static Player Player;
    public static CollectableBlock PlayerStartingBlock;

    public static float Timer;
    public static float PendingScore;

    private float _score;

    [SerializeField]
    private GameData gameData;

    #region Getters

    public static int CurrentLevel
    {
        get
        {
            return PlayerPrefs.GetInt(GameSingleton.currentLevelKey, 1);
        }
        set
        {
            PlayerPrefs.SetInt(GameSingleton.currentLevelKey, value);
        }
    }

    public static float Score
    {
        get
        {
            return Mathf.Floor(GameSingleton.GameManager._score);
        }
        set
        {
            GameSingleton.GameManager._score = value;
        }
    }


    public static float BestScore
    {
        get
        {
            return Mathf.Floor(PlayerPrefs.GetFloat(GameSingleton.bestScoreKey));
        }
        set
        {
            PlayerPrefs.SetFloat(GameSingleton.bestScoreKey, value);
        }
    }

    public static bool GameInputDown
    {
        get
        {
#if UNITY_EDITOR

            return GameManager.CheckState(GameState.Playing) && Input.GetKeyDown(KeyCode.UpArrow);

#else
            return GameManager.CheckState(GameState.Playing) && Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
#endif
        }
    }

    public static bool CheckState(GameState p_state)
    {
        return GameManager.GameState == p_state;
    }

    #endregion

    private void Awake()
    {
        GameSingleton.GameManager = this;
        GameSingleton.GameData = this.gameData;
    }

    private void Start()
    {
        GameManager.LoadLevel(GameManager.CurrentLevel);

        GameManager.GameState = GameState.Menu;
    }

    public static void LoadLevel(int p_level, Action p_callback = null)
    {
        if (SceneManager.sceneCount > 1 && GameManager.CurrentLevel > 0)
        {
            SceneManager.UnloadSceneAsync(GameManager.CurrentLevel);
        }

        GameManager.CurrentLevel = p_level;
        AsyncOperation _operation = SceneManager.LoadSceneAsync(p_level, LoadSceneMode.Additive);

        GameSingleton.CollectablesManager.Clear();

        _operation.completed += x =>
        {           

            GameManager.ResetPlayer();
            GameSingleton.CollectablesManager.CreateCollectables();

            p_callback?.Invoke();
        };
    }

    public static void SetUpLevel()
    {
        GameSingleton.CollectablesManager.SetUp();        
    }

    public static void SetState(GameState p_newState)
    {
        GameManager.GameState = p_newState;
    }

    public static void AddScore(float p_amount)
    {
        GameManager.Score += p_amount;
        UIManager.UpdateScore();
    }

    public static void StartGame()
    {
        CollectablesManager.StartCollectables();

        GameManager.ResetValues();
        GameManager.SetState(GameState.Playing);

        GameSingleton.AudioManager.PlaySound(AudioType.GameTheme, false);
    }

    public static void EndGame()
    {
        GameManager.AddScore(Mathf.Floor(GameManager.PendingScore));

        GameManager.SetState(GameState.GameOver);
        UIManager.ChangeScreen(ScreenType.GameOver);

        CollectablesManager.ResetCollectables();

        GameSingleton.AudioManager.PlaySound(AudioType.MenuTheme, false);
    }

    public static void ResetGame()
    {
        CollectablesManager.ResetCollectables();

        GameManager.ResetPlayer();
        GameManager.ResetValues();        
    }

    private static void ResetPlayer()
    {
        GameManager.PlayerStartingBlock = CollectablesManager.GetPlayerStartingBlock();
        Player.SetUp();
    }

    private static void ResetValues()
    {
        GameManager.Score = 0;
        GameManager.Timer = GameSingleton.GameData.playDuration;
    }

    private void Update()
    {
        if (GameManager.CheckState(GameState.Playing))
        {
            if(GameManager.Timer > 0)
            {
                GameManager.Timer -= Time.deltaTime;
                UIManager.UpdateTime();
            }
            else
            {
                GameManager.EndGame();
            }
        }
    }
}
