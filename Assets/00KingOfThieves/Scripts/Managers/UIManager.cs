﻿using System.Collections.Generic;
using UnityEngine;
using ZeptoLab.UI;

public enum ScreenType
{
    MainMenu,
    HUD,
    GameOver,
    LevelSelection
}

public class UIManager : MonoBehaviour
{
    private static List<GameScreen> _GameScreens = new List<GameScreen>();

    public static GameScreen CurrentScreen;
    public static Vector2 CoinTargetPosition;

    public static GameScreen GetScreen(ScreenType p_screenType)
    {
        return UIManager._GameScreens.Find(_x => _x.screenType == p_screenType);
    }

    public static T GetScreenScript<T>() where T : GameScreen
    {
        return (T)UIManager._GameScreens.Find(_x => _x.GetComponent<T>() != null);
    }

    private void Awake()
    {
        GameSingleton.UIManager = this;
    }

    private void Start()
    {
        GameSingleton.AudioManager.PlaySound(AudioType.MenuTheme, false);

        UIManager.ChangeScreen(ScreenType.MainMenu);
        UIManager.CoinTargetPosition = UIManager.GetScreenScript<HUD>().scorePosition;
    }

    public static void AddScreen(GameScreen p_screen)
    {
        if(UIManager.GetScreen(p_screen.screenType) == null)
        {
            UIManager._GameScreens.Add(p_screen);
        }
    }

    public static void ChangeScreen(ScreenType p_newScreen)
    {
        if(UIManager.CurrentScreen != null)
        {
            UIManager.CurrentScreen.ExitScreen();
        }

        UIManager.CurrentScreen = UIManager.GetScreen(p_newScreen);
        UIManager.CurrentScreen.EnterScreen();
    }

    public static void UpdateScore()
    {
        UIManager.GetScreenScript<HUD>().UpdateScore();
    }

    public static void UpdateTime()
    {
        UIManager.GetScreenScript<HUD>().UpdateTime();
    }
}
