﻿using System.Collections.Generic;
using UnityEngine;

public class CollectablesManager : MonoBehaviour
{
    private static List<Collectable> _PoolCollectables = new List<Collectable>();
    private static List<CollectableBlock> _CollectableBlocks = new List<CollectableBlock>();

    private static Collectable _Rand;

    public Transform collectablesPool;

    public static int CollectableBlocks => CollectablesManager._CollectableBlocks.Count;
    public static int ChestsInGame;

    public static bool HasPoolCollectables => CollectablesManager._PoolCollectables.Count > 0;

    private static bool CanSpawnChests => CollectablesManager.ChestsInGame < GameData.GetData<CollectablesData>().maxChestsInGame &&
                                          GameManager.Score >= GameData.GetData<CollectablesData>().minScoreForChestSpawn;

    public static CollectableBlock GetPlayerStartingBlock()
    {
        CollectableBlock _result = null;
        Transform _block;
        Vector2 _blockPosition;

        float _x = float.MaxValue;
        float _y = float.MaxValue;

        for (int i = 0; i < CollectablesManager._CollectableBlocks.Count; i++)
        {
            _block = CollectablesManager._CollectableBlocks[i].transform;
            _blockPosition = _block.localPosition;

            if (_blockPosition.x <= _x && _blockPosition.y <= _y)
            {
                _x = _blockPosition.x;
                _y = _blockPosition.y;

                _result = CollectablesManager._CollectableBlocks[i];
            }
        }

        return _result;
    }

    public static Collectable RandomCollectable()
    {
        CollectablesManager._Rand = CollectablesManager._PoolCollectables.Random();
        CollectablesManager.UnpoolCollectable(CollectablesManager._Rand);

        if (CollectablesManager._Rand.type == CollectableType.Chest)
        {
            if (CollectablesManager.CanSpawnChests)
            {
                return CollectablesManager._Rand;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return CollectablesManager._Rand;
        }
    }

    private void Awake()
    {
        GameSingleton.CollectablesManager = this;
        CollectablesManager._CollectableBlocks.Clear();
    }

    public void SetUp()
    {
        foreach (CollectableBlock f_block in CollectablesManager._CollectableBlocks)
        {
            f_block.SetUp();
        }

        CollectablesManager.ChestsInGame = 0;
    }

    public static void StartCollectables()
    {
        foreach (CollectableBlock f_block in CollectablesManager._CollectableBlocks)
        {
            f_block.StartBlock();
        }
    }

    public static void ResetCollectables()
    {
        foreach (CollectableBlock f_block in CollectablesManager._CollectableBlocks)
        {
            f_block.ResetBlock();
        }
    }

    public static void AddCollectableBlock(CollectableBlock p_block)
    {
        CollectablesManager._CollectableBlocks.Add(p_block);
    }

    public static void PoolCollectable(Collectable p_collectable)
    {
        CollectablesManager._PoolCollectables.Add(p_collectable);
    }

    public static void UnpoolCollectable(Collectable p_collectable)
    {
        CollectablesManager._PoolCollectables.Remove(p_collectable);
    }

    public void Clear()
    {
        this.ClearList(CollectablesManager._PoolCollectables);
        this.ClearList(CollectablesManager._CollectableBlocks);
    }

    private void ClearList<T>(List<T> p_list) where T : MonoBehaviour
    {
        if (p_list.Count > 0)
        {
            for (int i = 0; i < p_list.Count; i++)
            {
                if (p_list[i] != null)
                {
                    GameObject.Destroy(p_list[i].gameObject);
                }
            }

            p_list.Clear();
        }
    }

    public void CreateCollectables()
    {
        GameObject _prefab;
        GameObject _clone;
        Collectable _collectableItem;
        CollectableType _type;
        int _rand;

        this.ClearList(CollectablesManager._PoolCollectables);

        for (int i = 0; i < CollectablesManager.CollectableBlocks; i++)
        {
            _rand = Random.Range(0, 101);

            if (_rand <= GameData.GetData<CollectablesData>().GetData(CollectableType.Chest).spawnPercentage)
            {
                _type = CollectableType.Chest;
                _prefab = GameData.GetData<PrefabsData>().chest;
            }
            else
            {
                _type = CollectableType.Coin;
                _prefab = GameData.GetData<PrefabsData>().coin;
            }

            _clone = GameObject.Instantiate(_prefab, Vector3.zero, Quaternion.identity, this.collectablesPool);
            _collectableItem = _clone.GetComponent<Collectable>();
            _collectableItem.SetUp(_type);

            CollectablesManager.PoolCollectable(_collectableItem);

            _collectableItem.Toggle(false);
        }
    }
}
