﻿using UnityEngine;

public enum AudioType
{
    MenuTheme,
    GameTheme,
    Jump,
    Coin,
    Chest,
    ChestMoney,
    ButtonClick,
    Achievement,
    EnterScreen
}

public class AudioManager : MonoBehaviour
{
    private AudioData _data;

    [SerializeField]
    private AudioSource musicSource;
    [SerializeField]
    private AudioSource sfxSource;

    private AudioSource GetSource(AudioType p_audioType)
    {
        switch (p_audioType)
        {
            case AudioType.MenuTheme:
            case AudioType.GameTheme:
                return this.musicSource;

            default:
                return this.sfxSource;
        }
    }

    private void Awake()
    {
        GameSingleton.AudioManager = this;
        this._data = GameData.GetData<AudioData>();
    }

    public void PlaySound(AudioType p_type, bool p_oneShot = true)
    {
        this._data.GetSound(p_type).Play(this.GetSource(p_type), p_oneShot);
    }
}
