﻿using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private static Camera GameCamera => Camera.main;

    public static float HalfHeight => CameraManager.GameCamera.orthographicSize;
    public static float HalfWidth => CameraManager.GameCamera.aspect * CameraManager.HalfHeight;

    public static float HorizontalMin => -CameraManager.HalfWidth;
    public static float HorizontalMax => CameraManager.HalfWidth;

    public static float VerticalMin => -CameraManager.HalfHeight;
    public static float VerticalMax => CameraManager.HalfHeight;

    public static float Width => CameraManager.HorizontalMax * 2f;
    public static float Height => CameraManager.VerticalMax * 2f;

    public Camera guiCamera;

    [Space]

    public Transform topLimit;
    public Transform bottomLimit;
    public Transform leftLimit;
    public Transform rightLimit;

    public bool showLimits;

    private Vector3 _cameraPosition;

    private void Awake()
    {
        GameSingleton.CameraManager = this;

        this._cameraPosition = Vector3.zero;
        this._cameraPosition.z = Camera.main.transform.position.z;

        Camera.main.transform.position = this._cameraPosition;
    }

    private void Start()
    {
        this.topLimit.gameObject.SetActive(this.showLimits);
        this.bottomLimit.gameObject.SetActive(this.showLimits);
        this.leftLimit.gameObject.SetActive(this.showLimits);
        this.rightLimit.gameObject.SetActive(this.showLimits);

        if (showLimits)
        {
            this.topLimit.position = new Vector3(0, CameraManager.VerticalMax, 0);
            this.bottomLimit.position = new Vector3(0, CameraManager.VerticalMin, 0);
            this.leftLimit.position = new Vector3(CameraManager.HorizontalMin, 0, 0);
            this.rightLimit.position = new Vector3(CameraManager.HorizontalMax, 0, 0);
        }
    }

    public void ResizeCamera(float p_desiredWidth)
    {
        CameraManager.GameCamera.orthographicSize = p_desiredWidth / CameraManager.GameCamera.aspect;
        this.guiCamera.orthographicSize = CameraManager.GameCamera.orthographicSize / Screen.dpi;
    }
}
