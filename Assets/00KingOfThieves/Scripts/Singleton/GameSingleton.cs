using System;
using UnityEngine;

public class GameSingleton : Singleton<GameSingleton> 
{
	protected GameSingleton() {}

    #region CONSTANTS

    public const string bestScoreKey = "BestScore";
    public const string currentLevelKey = "CurrentLevel";
    
    #endregion

    #region PUBLIC_VARIABLES

    public static GameData GameData;
    public static GameManager GameManager;
    public static CollectablesManager CollectablesManager;
    public static UIManager UIManager;
    public static CameraManager CameraManager;
    public static AudioManager AudioManager;

    #endregion

    #region PROTECTED_VARIABLES

    #endregion

    #region PRIVATE_VARIABLES

    #endregion

    #region PUBLIC_COMMANDS

    #endregion

    #region PUBLIC_METHODS

    

    #endregion

    #region PRIVATE_METHODS
    
    #endregion
    
    #region PUBLIC_CLASSES
    
    [Serializable]
    public class CollectableItemData
    {
        public CollectableType type;
        public float spawnPercentage;
    }

    [Serializable]
    public class BlockColliderData
    {
        public BoxCollider2D collider;
        public bool active;

        public void SetUp()
        {
            Debug.Log($"Set To {active}");
            this.collider.gameObject.SetActive(this.active);
        }
    }

    [Serializable]
    public class GameSound
    {
        public AudioType type;
        public AudioClip clip;
        [Range(0,1f)]
        public float volume = 1f;

        public void Play(AudioSource p_source, bool p_oneShot = true)
        {
            if (p_oneShot)
            {
                p_source.PlayOneShot(this.clip, this.volume);
            }
            else
            {
                p_source.clip = this.clip;
                p_source.volume = this.volume;

                p_source.Play();
            }
        }
    }

    [Serializable]
    public class LevelData
    {
        public Sprite image;
        public string name;
        public int index;
    }

    #endregion
}

#region ENUM


#endregion
