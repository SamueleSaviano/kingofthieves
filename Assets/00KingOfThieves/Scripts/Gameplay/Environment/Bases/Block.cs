﻿using UnityEngine;

public class Block : MonoBehaviour
{
    protected SpriteRenderer _rend => this.GetComponent<SpriteRenderer>();

    public virtual void SetSprite(Sprite p_sprite)
    {
        this._rend.sprite = p_sprite;
    }
}
