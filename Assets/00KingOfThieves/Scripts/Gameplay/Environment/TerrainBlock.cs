﻿using UnityEngine;

public class TerrainBlock : Block
{
    public GameObject topCollider;
    public GameObject bottomCollider;
    public GameObject leftCollider;
    public GameObject rightCollider;

    public void SetColliders(bool p_top, bool p_bottom, bool p_left, bool p_right)
    {
        this.topCollider.SetActive(p_top);
        this.bottomCollider.SetActive(p_bottom);
        this.leftCollider.SetActive(p_left);
        this.rightCollider.SetActive(p_right);
    }
}
