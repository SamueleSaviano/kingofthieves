﻿using System.Collections;
using UnityEngine;

public class CollectableBlock : Block
{
    //[HideInInspector]
    public Collectable collectable;

    private CollectablesData _data => GameData.GetData<CollectablesData>();

    private float _distanceFromPlayer => Vector2.Distance(this.transform.position, GameManager.Player.transform.position);
    private bool _canSpawn => this.transform.childCount == 0 && _distanceFromPlayer > this.transform.localScale.x * 2f && CollectablesManager.HasPoolCollectables;

    private void OnEnable()
    {
        CollectablesManager.AddCollectableBlock(this);
    }

    public void SetUp()
    {
        this.Activate();
    }

    public void StartBlock()
    {
        if (this.collectable)
        {
            this.collectable.StartCollectable();
        }
        else
        {
            this.RetryActivate();
        }
    }

    public void ResetBlock()
    {
        StopAllCoroutines();

        if (this.collectable)
        {
            this.collectable.ResetCollectable();
        }
    }

    private void Activate()
    {
        if (this._canSpawn && Random.value > 0.5f)
        {
            this.collectable = CollectablesManager.RandomCollectable();

            if (this.collectable != null)
            {
                if (this.collectable.type == CollectableType.Chest)
                {
                    CollectablesManager.ChestsInGame++;
                }
                
                this.collectable.Activate(this);
            }
            else
            {
                this.Activate();
            }
        }
        else
        {
            this.RetryActivate();
        }
    }

    public void RetryActivate(bool p_wait = true)
    {
        if (this.collectable == null)
        {
            StartCoroutine(this.RetryActivateCO(p_wait));
        }
    }

    private IEnumerator RetryActivateCO(bool p_wait)
    {
        if (p_wait)
        {
            yield return new WaitForSeconds(Random.Range(this._data.minSpawnTime, this._data.maxSpawnTime));
        }

        if (GameManager.CheckState(GameState.Playing))
        {
            this.Activate();
        }
        else
        {
            StartCoroutine(this.RetryActivateCO(p_wait));
        }
    }
}
