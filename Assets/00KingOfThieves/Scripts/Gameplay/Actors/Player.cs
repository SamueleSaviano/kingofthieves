﻿using System;
using UnityEngine;

public enum PlayerState
{
    Idling,
    Walking,
    Jumping,
    Sliding,
    Falling
}

public class Player : MonoBehaviour
{
    [HideInInspector]
    public PlayerState state;

    private Rigidbody2D _rb;
    private BoxCollider2D _collider;
    private SpriteRenderer _rend;
    private Animator _animator;

    private RaycastHit2D[] _leftHits = new RaycastHit2D[Player.raysPerSide];
    private RaycastHit2D[] _rightHits = new RaycastHit2D[Player.raysPerSide];
    private RaycastHit2D[] _bottomtHits = new RaycastHit2D[Player.raysPerSide];

    private Collectable _collidedCollectable;

    private Vector2 _direction;
    private Vector2 _velocity;
    private Vector2 _origin;

    private Vector2 _originalFrictionPos;
    private Vector2 _currentFrictionPos;
    private Vector3 _originalFrictionRot;
    private Vector3 _currentFrictionRot;

    private const int raysPerSide = 5;
    private const float timeBeforeSliding = 0.1f;
    private const float inputTime = 0.1f;
    private const float minDistanceForTouching = 1f;

    private int _collidingLayer;
    private float _raycastLength;
    private float _slidingTimer;
    private float _inputTimer;

    private bool _inputReceived;

    [SerializeField]
    private SpriteRenderer wallFriction;

    [Space]

    [Header("Movements")]
    [Range(0, 10f)]
    public float jumpSpeed;
    [Range(0, 10f)]
    public float walkSpeed;
    [Range(0.1f, 10f)]
    public float wallGrip;

    [Header("Physics")]
    [Range(-0.5f, 0.5f)]
    public float raysOriginOffsetY;
    [Range(0.1f, 1f)]
    public float sideRaysLength;
    [Range(0.1f, 1f)]
    public float sideRaysScaleFactor;

    [Space]

    [Range(-0.5f, 0.5f)]
    public float raysOriginOffsetX;
    [Range(0.1f, 1f)]
    public float bottomRaysLength;
    [Range(0.1f, 1f)]
    public float bottomRaysScaleFactor;

    [Space]

    [SerializeField]
    private bool _drawBottomRays;
    [SerializeField]
    private bool _drawLeftRays;
    [SerializeField]
    private bool _drawRightRays;


    #region Getters

    private LayersData _layersData => GameData.GetData<LayersData>();

    private float _walkDirection => this._direction == Vector2.right ? this.walkSpeed : -this.walkSpeed;

    private float _bottomRaysScale => this.transform.localScale.x * this.bottomRaysScaleFactor;
    private float _sideRaysScale => this.transform.localScale.y * this.sideRaysScaleFactor;

    private float _frictionPosMultiplier(Vector2 p_direction) { return p_direction == Vector2.left ? 1f : -1f; }

    private bool _isOnWall => this._leftHit || this._rightHit;
    private bool _isInAir => !this._isOnGround && !this._isOnWall;
    private bool _canChangeDirection => (this._leftHit && this._direction == Vector2.left) || (this._rightHit && this._direction == Vector2.right);

    private bool _isOnGround
    {
        get
        {
            for (int i = 0; i < this._bottomtHits.Length; i++)
            {
                if (this._bottomtHits[i] && this.IsInRange(Vector2.down, this._bottomtHits[i].collider))
                {
                    return true;
                }
            }

            return false;
        }
    }

    private bool _leftHit
    {
        get
        {
            for (int i = 0; i < this._leftHits.Length; i++)
            {
                if (this._leftHits[i] && this.IsInRange(Vector2.left, this._leftHits[i].collider))
                {
                    return true;
                }
            }

            return false;
        }
    }

    private bool _rightHit
    {
        get
        {
            for (int i = 0; i < this._rightHits.Length; i++)
            {
                if (this._rightHits[i] && this._collider.IsTouching(this._rightHits[i].collider))
                {
                    return true;
                }
            }

            return false;
        }
    }

    private bool IsInRange(Vector2 p_direction, Collider2D p_other)
    {
        if (p_direction == Vector2.down) 
        {
            return Vector2.Distance(this.transform.position, p_other.transform.position) <= this.transform.localScale.y * 0.5f + Player.minDistanceForTouching;
        }        
        else if(p_direction == Vector2.right || p_direction == Vector2.left)
        {
            return Vector2.Distance(this.transform.position, p_other.transform.position) <= this.transform.localScale.x * 0.5f + Player.minDistanceForTouching;
        }

        return false;
    }

    #endregion

    private void Awake()
    {
        GameManager.Player = this;

        this._rb = this.GetComponent<Rigidbody2D>();
        this._collider = this.GetComponent<BoxCollider2D>();
        this._rend = this.GetComponent<SpriteRenderer>();
        this._animator = this.GetComponent<Animator>();

        this._originalFrictionPos = this.wallFriction.transform.localPosition;
        this._originalFrictionRot = this.wallFriction.transform.localEulerAngles;

        this.Toggle(false);
        this.ToggleWallFriction(false);
    }

    private void Start()
    {
        this.SetUp();

    }

    public void SetUp()
    {
        this.SetState(PlayerState.Idling);

        this.transform.position = GameManager.PlayerStartingBlock.transform.position;

        this._direction = Vector2.right;

        this._slidingTimer = 0;
        this._inputTimer = 0;
        this._inputReceived = false;

        this.Toggle(true);
    }

    public void Toggle(bool p_enable)
    {
        this.gameObject.SetActive(p_enable);
    }

    private void ToggleWallFriction(bool p_enable, Vector2 p_direction = default)
    {
        if (this.wallFriction.gameObject.activeSelf != p_enable)
        {
            this.wallFriction.gameObject.SetActive(p_enable);

            if (p_enable)
            {
                this.SetWallFriction(p_direction);
            }
        }
    }

    private void SetWallFriction(Vector2 p_direction)
    {
        this._currentFrictionPos.x = this._originalFrictionPos.x * this._frictionPosMultiplier(p_direction);
        this._currentFrictionPos.y = this._originalFrictionPos.y;

        this._currentFrictionRot.z = this._originalFrictionRot.z * this._frictionPosMultiplier(p_direction);

        this.wallFriction.transform.localPosition = this._currentFrictionPos;
        this.wallFriction.transform.localEulerAngles = this._currentFrictionRot;
        this.wallFriction.flipX = p_direction == Vector2.right;
    }

    private void SetState(PlayerState p_newState)
    {
        if (this.state != p_newState)
        {
            this._animator.ResetTrigger(this.state.ToString());

            this.state = p_newState;

            this._animator.SetTrigger(this.state.ToString());

            if(this.state != PlayerState.Sliding)
            {
                this._rend.flipX = this._direction == Vector2.left;
                this.ToggleWallFriction(false);
            }
            else
            {
                this._rend.flipX = this._rightHit;
            }
        }
    }

    private RaycastHit2D CastRay(Vector2 p_direcion, int p_index, LayerMask p_layerMask, bool p_drawRay = false, Color p_debugColor = default)
    {
        if (p_direcion == Vector2.down)
        {
            this._raycastLength = this.transform.localScale.y * this.bottomRaysLength;
            this._origin.x = ((this.transform.position.x + this.raysOriginOffsetX) - (this._bottomRaysScale * 0.5f)) + ((this._bottomRaysScale / (Player.raysPerSide - 1)) * p_index);
            this._origin.y = this.transform.position.y;
        }
        else
        {
            this._raycastLength = this.transform.localScale.x * this.sideRaysLength;
            this._origin.x = this.transform.position.x;
            this._origin.y = ((this.transform.position.y + this.raysOriginOffsetY) - (this._sideRaysScale * 0.5f)) + ((this._sideRaysScale / (Player.raysPerSide - 1)) * p_index);
        }

        if (p_drawRay)
        {
            Debug.DrawRay(_origin, p_direcion * this._raycastLength, p_debugColor, 3f);
        }

        return Physics2D.Raycast(_origin, p_direcion, this._raycastLength, p_layerMask);
    }

    private void FixedUpdate()
    {
        for (int i = 0; i < Player.raysPerSide; i++)
        {
            this._bottomtHits[i] = this.CastRay(Vector2.down, i, this._layersData.terrainLayer, this._drawBottomRays, Color.green);
        }

        for (int i = 0; i < Player.raysPerSide; i++)
        {
            this._leftHits[i] = this.CastRay(Vector2.left, i, this._layersData.wallsLayer, this._drawLeftRays, Color.red);
        }

        for (int i = 0; i < Player.raysPerSide; i++)
        {
            this._rightHits[i] = this.CastRay(Vector2.right, i, this._layersData.wallsLayer, this._drawRightRays, Color.blue);
        }
    }

    private void Update()
    {
        if (GameManager.CheckState(GameState.Playing))
        {
            if (this._isOnGround && !this._isOnWall)
            {
                this.SetState(PlayerState.Walking);
                this.SetVelocity(this._walkDirection, 0);
            }

            if (this._isOnWall && !this._isOnGround)
            {
                this.SetState(PlayerState.Sliding);
                this._slidingTimer += Time.deltaTime;

                if (this._slidingTimer >= Player.timeBeforeSliding)
                {
                    this.ToggleWallFriction(true, this._rightHit ? Vector2.right : Vector2.left);
                    this.SetVelocity(this._direction.x, -this.wallGrip);
                }
            }

            if (this._isOnGround && this._isOnWall)
            {
                this.SetState(PlayerState.Idling);

                if (this._slidingTimer > 0)
                {
                    this._slidingTimer = 0;
                }
            }

            if (GameManager.GameInputDown)
            {
                this._inputTimer = 0;
                this._inputReceived = true;
            }

            if (this._inputReceived)
            {
                this._inputTimer += Time.deltaTime;

                if(this._inputTimer >= Player.inputTime)
                {
                    this._inputReceived = false;
                }

                if (!this._isInAir)
                {
                    GameSingleton.AudioManager.PlaySound(AudioType.Jump);

                    if (this._isOnWall)
                    {
                        this.ChangeDirection();
                    }

                    this.SetState(PlayerState.Jumping);
                    this.SetVelocity(this._walkDirection, this.jumpSpeed);
                }
                else
                {
                    this._velocity.y = 0;
                }

                if(this._inputTimer >= Player.inputTime)
                {
                    this._inputReceived = false;
                }
            }

            if(this._isInAir && this.state != PlayerState.Jumping)
            {
                this.SetState(PlayerState.Falling);
            }
        }
        else
        {
            this.SetState(PlayerState.Idling);
        }
    }

    private void SetVelocity(float p_x, float p_y)
    {
        this._velocity.x = p_x;
        this._velocity.y = p_y;
        this._rb.velocity = this._velocity;
    }

    private void ChangeDirection()
    {
        if (this._canChangeDirection)
        {
            this._direction = (this._direction == Vector2.right) ? Vector2.left : Vector2.right;
        }
    }

    private void CheckCollision(Collider2D p_other, LayerMask p_layer, Action p_callback, bool p_conditions = true)
    {
        this._collidingLayer = p_other.gameObject.layer;

        if(this._collidingLayer.CheckLayer(p_layer) && p_conditions)
        {
            p_callback?.Invoke();
        }
    }

    private void CheckCollectable(GameObject p_go)
    {
        this._collidedCollectable = p_go.GetComponent<Collectable>();
        this._collidedCollectable.Collect();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        this.CheckCollision(collision, this._layersData.coinLayer, () => { this.CheckCollectable(collision.gameObject); });
        this.CheckCollision(collision, this._layersData.chestLayer, () => { this.CheckCollectable(collision.gameObject); });
    }
}
