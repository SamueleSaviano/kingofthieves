﻿using UnityEngine;

public class Coin : Collectable
{
    private BoxCollider2D _collider;

    private Vector2 _startPosition;
    private Vector2 _targetPosition => UIManager.GetScreenScript<HUD>().scorePosition;

    public float moveSpeed;

    private float _lerpT;
    private bool _canMove;

    private float _speed;

    private void Awake()
    {
        this._collider = this.GetComponent<BoxCollider2D>();
    }

    public override void SetUp(CollectableType p_type)
    {
        //this._targetPosition = UIManager.CoinTargetPosition;
        this._canMove = false;
        this._lerpT = 0;

        base.SetUp(p_type);
    }

    public override void Toggle(bool p_enable)
    {
        base.Toggle(p_enable);
    }

    public override void Collect(float p_additionalScore = 0)
    {
        base.PlaySound();
        this.GoToHUD();
    }

    public void GoToHUD()
    {
        if (!this._canMove)
        {
            GameManager.PendingScore += this.scoreAmount;

            this._collider.enabled = false;

            this._startPosition = this.transform.position;
            this._speed = this.moveSpeed / Vector2.Distance(this._startPosition, this._targetPosition);

            this._canMove = true;
        }
    }

    private void Update()
    {
        if (this._canMove)
        {
            this._lerpT += Time.deltaTime * this._speed;
            this.transform.position = Vector2.Lerp(this._startPosition, this._targetPosition, this._lerpT);

            if(this._lerpT >= 1f)
            {
                this._lerpT = 0;
                this._canMove = false;
                this._collider.enabled = true;

                base.Collect();

                GameManager.PendingScore -= this.scoreAmount;
            }
        }
    }
}
