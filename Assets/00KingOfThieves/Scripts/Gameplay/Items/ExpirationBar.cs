﻿using UnityEngine;

public class ExpirationBar : MonoBehaviour
{
    [SerializeField]
    private Color _startingColor;
    [SerializeField]
    private Color _targetColor;

    [SerializeField]
    private Transform _mask;

    [SerializeField]
    private float _colorLerpMultiplier;

    private SpriteRenderer _rend;

    private Color _barColor;
    private Vector3 _maskPosition;
    private Vector3 _targetMaskPosition;

    private float _startingTime;
    private float _timer;

    private bool _canRun;

    private float _targetPosition => -this.transform.localScale.x;
    private float _lerpT => Mathf.InverseLerp(this._startingTime, 0, this._timer);

    private void Awake()
    {
        this._rend = this.GetComponent<SpriteRenderer>();
    }

    private void OnEnable()
    {
        this._mask.localPosition = Vector3.zero;
    }

    public void SetUp(float p_time)
    {
        this._startingTime = p_time;
        this._timer = p_time;

        this._targetMaskPosition = Vector3.zero;
        this._targetMaskPosition.x = this._targetPosition;

        this._canRun = p_time > 0;
    }

    private void Update()
    {
        if (this._canRun)
        {
            this._timer -= Time.deltaTime;

            this._barColor = Color.Lerp(this._startingColor, this._targetColor, this._lerpT * this._colorLerpMultiplier);
            this._rend.color = this._barColor;

            this._maskPosition = Vector3.Lerp(Vector3.zero, this._targetMaskPosition, this._lerpT);
            this._mask.transform.localPosition = this._maskPosition;

            if(this._timer <= 0)
            {
                this._canRun = false;
            }
        }
    }
}
