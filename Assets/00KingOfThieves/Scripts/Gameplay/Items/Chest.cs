﻿using System.Collections;
using UnityEngine;

public class Chest : Collectable
{
    private Animator _animator;

    [Space]

    [SerializeField]
    private ParticleSystem _coinParticles;

    public float totalPercentage;

    private bool _isPlaying;

    private float _scoreAmount => GameManager.Score.Percentage(this.totalPercentage);

    private void Awake()
    {
        this._animator = this.GetComponent<Animator>();
    }

    public override void Activate(CollectableBlock p_block)
    {
        this._isPlaying = false;
        base.Activate(p_block);
    }

    public override void SetUp(CollectableType p_type)
    {
        base.SetUp(p_type);
    }

    public override void Toggle(bool p_enable)
    {
        base.Toggle(p_enable);
    }

    public override void Collect(float p_additionalScore = 0)
    {
        if (!this._isPlaying)
        {
            this._isPlaying = true;

            base.PlaySound();

            this._animator.SetTrigger("Open");
            GameManager.PendingScore += this._scoreAmount;
        }
    }

    protected override void Expire()
    {
        base.Expire();
        CollectablesManager.ChestsInGame--;
    }

    protected override void ResetToPool()
    {
        base.ResetToPool();
    }

    public void PlayParticle()
    {
        this._coinParticles.Play();
        StartCoroutine(this.WaitForCollecting());
    }

    private IEnumerator WaitForCollecting()
    {
        yield return new WaitForSeconds(this._coinParticles.main.duration * 2f);

        base.Collect(_scoreAmount);
        CollectablesManager.ChestsInGame--;
        GameManager.PendingScore -= this._scoreAmount;
    }
}
