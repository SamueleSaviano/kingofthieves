﻿using System.Collections;
using UnityEngine;
using static GameSingleton;

public enum CollectableType
{
    Coin,
    Chest
}

public class Collectable : MonoBehaviour
{
    protected CollectableItemData _data;
    protected CollectableBlock _parentBlock;

    [HideInInspector]
    public CollectableType type;

    [SerializeField]
    private AudioType[] _collectSounds;

    [SerializeField]
    protected ExpirationBar expirationBar;

    [Space]

    public float scoreAmount;
    public float expirationTime;

    protected bool _collected;

    public virtual void StartCollectable()
    {
        if (this.expirationTime > 0)
        {
            this.expirationBar.SetUp(this.expirationTime);
            StartCoroutine(this.WaitForExpiration());
        }
    }

    public virtual void ResetCollectable()
    {
        StopAllCoroutines();
        this.ResetToPool();
    }

    public virtual void SetUp(CollectableType p_type)
    {
        this._data = GameData.GetData<CollectablesData>().GetData(p_type);
        this.type = this._data.type;

        this._collected = false;
    }

    public virtual void Toggle(bool p_enable)
    {
        this.gameObject.SetActive(p_enable);        
    }

    public virtual void Activate(CollectableBlock p_block)
    {
        this._parentBlock = p_block;

        this.transform.SetParent(this._parentBlock.transform);
        this.transform.localPosition = Vector2.zero;
        this.Toggle(true);

        this.StartCollectable();

        this._collected = false;
    }

    protected virtual IEnumerator WaitForExpiration()
    {
        yield return new WaitForSeconds(this.expirationTime);
        this.Expire();
    }

    protected virtual void Expire()
    {
        this.ResetToPool();
    }

    public virtual void Collect(float p_additionalScore = 0)
    {
        if (!this._collected)
        {
            this._collected = true;

            GameManager.AddScore(this.scoreAmount + p_additionalScore);
            this.ResetToPool();
        }
    }

    protected virtual void PlaySound()
    {
        for (int i = 0; i < this._collectSounds.Length; i++)
        {
            GameSingleton.AudioManager.PlaySound(this._collectSounds[i]);
        }
    }

    protected virtual void ResetToPool()
    {
        CollectablesManager.PoolCollectable(this);

        this.Toggle(false);
        this.transform.SetParent(GameSingleton.CollectablesManager.collectablesPool);
        this.transform.localPosition = Vector2.zero;

        this._parentBlock.collectable = null;

        if (GameManager.CheckState(GameState.Playing))
        {
            this._parentBlock.RetryActivate();
        }

        this._parentBlock = null;
    }
}
