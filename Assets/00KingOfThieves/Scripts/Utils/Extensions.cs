﻿using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static float Percentage(this float p_value, float p_percentage)
    {
        return p_value / 100f * p_percentage;
    }

    public static string TimeString(this float p_time)
    {
        return $"{(p_time / 60f).ToString("00")}:{(p_time % 60f).ToString("00")}";
    }

    public static bool CheckLayer(this int p_layer, LayerMask p_layerToCheck)
    {
        int _mask = LayersData.GetMask(p_layer);
        return _mask == p_layerToCheck.value;
    }

    public static T Random<T>(this List<T> p_list)
    {
        return p_list[UnityEngine.Random.Range(0, p_list.Count)];
    }
}
